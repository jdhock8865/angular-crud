Angular Notes

Source for element images: https://creazilla.com/nodes/7874933-mercury-element-clipart

Creating Components
    ng generate component component-folder/component-name

Angular Operators
=:      Assign a value
==:     Compare two values
===:    Compare two values and their types

Types of Data Binding
    one way data binding:   from component to view template
    one way data binding:   from view template to component
    two way data binding:   from component to view template and view template to component

    One Way Data Binding: from component to view template
        use interpolation: <img src='{{imagePath}}'/>
        use property binding: <img [src] = 'imagePath'/>

    Interpolation is a special syntax that Angular converts into a property binding.
    Property Binding must be used to concatenate strings
    Property Binding must be used to set an element property to a non-string data value
    Property Binding requires square brackets []

    interpolation example:      <button disabled='{{isDisabled}}'>Click Me</button>
    property binding example:   <button [disabled]='isDisabled'>Click Me</button>

    HTML Attribute vs DOM Property
        DOM containes the HTML elements as objects, their properties, methods and events and it is a standard for
        accessing, modifying, adding or deleting HTML elements.
        Attributes are defined by HTML. where as properties are defined by the DOM
        Attributes initialize DOM properties. Once the initialization complete, the attributes job is done.
        Property values can change, where as attribute value can not change.

    Angular binding works with properties and events, and not attributes

Angular Attribute Binding
    <th colspan="2">Employee Details</th>  colspan is an attribute of the the th element

    Example
    QComponent({
        ...
    })
    export class EmployeeComponent {
        columnSpan: number = 2;
    }

    using interpolation:
    <th colspan="{{colspan}}">Employee Details</th>  colspan is an attribute of the the th element
    results: Template Parse Error

    using attribute binding with interpolation style syntax:
    <th attr.colspan="{{colspan}}">Employee Details</th>  colspan is an attribute of the the th element
    results: using the attr prefix in front of the attribute results in no errors.

    using attribute binding with property binding style syntax:
    <th [attr.colspan]="colspan">Employee Details</th>  colspan is an attribute of the the th element
    results: works just like attribute binding

    What is Attribute Binding?
        Interpoolation and Property Biding deal with binding Componenet class properties to HTML element properties and NOT Attributes.
        Not all HTML elements attributes have corresponding properties. For example, colspan attribute doe not have a corresponding property.
        For elements that have attributes with no corresponding properties we need to be able to bind to HTML element attributes.
        Angular provided Attribute Binding.

Class Binding in Angular  
    styles.css
        .boldClass{
            font-weight:bold;
        }
        .italicsClass{
            font-style:italic
        }
        .colorClass{
            color:red
        }

    app.component.ts
        @Component{{
            template:`<button class="colorClass" [class]='classesToApply'>MyButton</button>`
        }}
        export class AppComponent{
            classesToApply: string = 'italicsClass boldClass';
        }

    Adding or Removing a single class
        @Component{{
            template:   '<button class="colorClass" [class]='classesToApply'>MyButton</button>
                        <br/><br/>
                        <button class="colorClass italicsClass boldClass" [class.boldClass]='classesToApply'>MyButton</button>

        }}
        export class AppComponent{
            classesToApply: string = 'italicsClass boldClass';
            applyBoldClass: boolean = true;
        }

    Adding or Removing multiple classes using the ngClass directive
        @Component{{
            template:   '<button class="colorClass" [class]='classesToApply'>MyButton</button>
                        <br/><br/>
                        <button class="colorClass italicsClass boldClass" [class.boldClass]='classesToApply'>MyButton</button>
                        <br/><br/>
                        <button class="colorClass" [ngClass]='addClasses'>MyButton</button>`

        }}
        export class AppComponent{
            classesToApply: string = 'italicsClass boldClass';
            applyBoldClass: boolean = true;
            applyItalicsClass: boolean = true;

            addClasses() {
                let classes = {
                    boldClass: this.applyBoldClass,
                    italicsClass: this.applyItalicsClass
                }

                return classes;
            }
        }

Style Binding in Angular
    @Component{{
        template:   '<button style=color:red' [style.font-weight]="isBold ? 'bold' : 'normal'">MyButton</button>`
        or
        template:   '<button style=color:red' [style.fontWeight]="isBold ? 'bold' : 'normal'">MyButton</button>`

        template:   '<button style=color:red' [style.font-weight]="isBold ? 'bold' : 'normal'">MyButton</button>
                    <br/><br/>
                    <button style=color:red' [style.font-size.px]="fontSize">MyButton</button>`

    }}
    export class AppComponent{
        isBold: boolean = true;
        fontSize: number = 30;
    }

    Adding or Removing multiple classes using the ngStyle directive
        @Component{{
            template:   '<button style=color:red' [ngStyle]=addStyles()>MyButton</button>`
        }}
        export class AppComponent{
            isBold: boolean = true;
            fontSize: number = 30;
            isItalic: boolean = true;


            addCSSClasses() {
                let styles = {
                    'font-size.px': this.fontSize,
                    'font-style': this.isItalic ? 'italic' : 'normal',
                    'font-weight': this.isBold ? 'bold': 'normal
                };

                return styles
            }
        }

Angular Event Binding
    The event, click, is enclosed in paranthesis and set equal to a method defined in the class
    @Component({
        selector: 'app-create-employee',
        templateUrl: './create-employee.component.html',
        styleUrls: ['./create-employee.component.css']
    })
    export class CreateEmployeeComponent implements OnInit{
        firstName!: string;
        middleInitial!: string;
        lastName!: string;
        email!: string;
        phoneNumber!: string;
        dateOfBirth!: Date;
        department!: string;
        active!: boolean;

        hideDateOfBirth: boolean = false;

        constructor () {}

        ngOnInit() {}

        toggleDateOfBirth(): void{
            this.hideDateOfBirth = !this.hideDateOfBirth;
        }
    }

    <button (click)='toggleDateOfBirth()' class="btn-primary" [ngClass]='addCSSClasses()'>{{hideDateOfBirth? 'Hide' : 'Show' }} Date of Birth</button>

Two way data binding
    A combination of property binding and event binding
    The square brackets on the outside are for property binding
    The paranthesis are for event binding
    
    <input id="firstName" name="firstName" [(ngModel)]="firstName" type="text" class="form-control"> 


Angular Pipes 
    Pipes transform data before display

Continer and Nested Components
    see code 

Angular component lifecycle hooks
    ngOnChanges: Executes every time the value of an input property changes. The hook method receives a SimpleChanges object containing current and previou
    property values.  This called before ngOnInit.

    ngOnInit: Executes after the constructor and after the ngOnChange hook for the first time.  It is most commonly used for componenet initialisation and
    retrieving data from a database.

    ngOnDestroy: Executes just before angular destroys the component and generally used for performing cleanup.

Angular Services
    A service in Angular is generally used when you need to reuse data or logic across multiple components

Promises vs Observables
Promises:
    emit single value
    not lazy
Observables:
    emit multiple values over a period of time
    Lazy An observable is not called until we subscribe to the Observable

Custom Validators
    refer to this stackoverflow when encountering issues with custom validators: https://stackoverflow.com/questions/70106472/property-fname-comes-from-an-index-signature-so-it-must-be-accessed-with-fn


