import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { CommonModule } from '@angular/common';

import { HomePageComponent } from '../components/home-component/home-page/home-page.component';
import { PageNotFoundComponent } from '../components/home-component/page-not-found/page-not-found.component';

const appRoutes: Routes = [
  {path: 'home', component:HomePageComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [],
  imports: [BrowserModule, RouterModule.forRoot(appRoutes), FormsModule, HttpClientModule, CommonModule],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule { }