import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { ListElementsComponent } from '../components/element-component/list-elements/list-elements.component';
import { CreateElementComponent } from '../components/element-component/create-element/create-element.component';
import { ElementDetailsComponent } from '../components/element-component/element-details/element-details.component';

import { ListEmployeesComponent } from '../components/employee-component/list-employees/list-employees.component';
import { CreateEmployeeComponent } from '../components/employee-component/create-employee/create-employee.component';

import { HomePageComponent } from '../components/home-component/home-page/home-page.component';
import { PageNotFoundComponent } from '../components/home-component/page-not-found/page-not-found.component';


const appRoutes: Routes = [
  {path: 'home', component:HomePageComponent},
  {path: 'elements', component:ListElementsComponent},
  {path: 'createElements', component: CreateElementComponent},
  {path: 'employees', component:ListEmployeesComponent},
  {path: 'createEmployees', component: CreateEmployeeComponent},
  {path: 'elements/:atomic_number', component: ElementDetailsComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
  
];

@NgModule({
  declarations: [],
  imports: [BrowserModule, RouterModule.forRoot(appRoutes), FormsModule, HttpClientModule],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
