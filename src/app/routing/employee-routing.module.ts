import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { CommonModule } from '@angular/common';

import { ListEmployeesComponent } from '../components/employee-component/list-employees/list-employees.component';
import { CreateEmployeeComponent } from '../components/employee-component/create-employee/create-employee.component';

const appRoutes: Routes = [
  {path: 'employees', component:ListEmployeesComponent},
  {path: 'createEmployee', component: CreateEmployeeComponent},
];

@NgModule({
  declarations: [],
  imports: [BrowserModule, RouterModule.forRoot(appRoutes), FormsModule, HttpClientModule, CommonModule],
  exports: [RouterModule],
  providers: []
})
export class EmployeeRoutingModule { }