import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { CommonModule } from '@angular/common';

import { ListElementsComponent } from '../components/element-component/list-elements/list-elements.component';
import { CreateElementComponent } from '../components/element-component/create-element/create-element.component';
import { ElementDetailsComponent } from '../components/element-component/element-details/element-details.component';

const appRoutes: Routes = [
  {path: 'elements', component:ListElementsComponent},
  {path: 'createElement', component: CreateElementComponent},
  {path: 'elements/:atomic_number', component: ElementDetailsComponent},
];

@NgModule({
  declarations: [],
  imports: [BrowserModule, RouterModule.forRoot(appRoutes), FormsModule, HttpClientModule, CommonModule],
  exports: [RouterModule],
  providers: []
})
export class ElementRoutingModule { }