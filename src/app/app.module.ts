// source tutorial: https://www.youtube.com/playlist?list=PL6n9fhu94yhWqGD8BuKuX-VTKqlNBj-m6
// kudvenkat: Angular 2 tutorial for beginners

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './routing/app-routing.module';
import { ElementRoutingModule } from './routing/element-routing.module';
import { EmployeeRoutingModule } from './routing/employee-routing.module';
import { HomeRoutingModule } from './routing/home-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
//import { SelectRequiredValidatorDirective } from './shared/directives/select-required-validator.directive';


@NgModule({
  declarations: [
    AppComponent,
    //SelectRequiredValidatorDirective
  ],
  imports: [
    BrowserModule,
    ElementRoutingModule,
    EmployeeRoutingModule,
    HomeRoutingModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
