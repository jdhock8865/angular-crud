import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalRadioComponent } from './global-radio.component';

describe('GlobalRadioComponent', () => {
  let component: GlobalRadioComponent;
  let fixture: ComponentFixture<GlobalRadioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ GlobalRadioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GlobalRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
