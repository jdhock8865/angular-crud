import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-global-radio',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './global-radio.component.html',
  styleUrls: ['./global-radio.component.css']
})
export class GlobalRadioComponent {

}
