import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-global-dropdown',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './global-dropdown.component.html',
  styleUrls: ['./global-dropdown.component.css']
})
export class GlobalDropdownComponent {

}
