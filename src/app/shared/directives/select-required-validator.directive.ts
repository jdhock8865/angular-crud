import { Directive } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from "@angular/forms";

@Directive({
    standalone: true,
    selector: '[appSelectValidator]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: SelectRequiredValidatorDirective,
        multi: true
    }]
})
export class SelectRequiredValidatorDirective implements Validator{

    validate(control: AbstractControl<any, any>): ValidationErrors | null {
        console.log('Validating the department dropdown on the create employee form!');
        return control.value ==='-1' ? {'defaultSelected': true} : null;
    }

    registerOnValidatorChange?(fn: () => void): void {
        throw new Error("Method not implemented.");
    }
    
}