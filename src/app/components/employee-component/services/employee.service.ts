import { Injectable } from '@angular/core';
import { IEmployee } from '../interfaces/employee';
import { IDepartment } from '../interfaces/department';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { 
    console.log("A new instance of the get employee service has been created.")
  }

  private listOfEmployees: IEmployee[] = [
    {
      employeeId: "1",
      firstName: 'John',
      middleInitial: 'D',
      lastName: 'Hock',
      email: 'jdhock8865@gmail.com',
      phoneNumber: '980-745-5654',
      contactPreference: 'phone',
      dateOfBirth: new Date('08/08/1965'),
      department: 1,
      salary: 55236.80,
      isActive: true,
      imageUrl: 'assets/images/male_01.jpg'
    },
    {
      employeeId: "2",
      firstName: 'Heather',
      middleInitial: 'A',
      lastName: 'Williams',
      email: 'hawilliams8674@gmail.com',
      phoneNumber: '980-745-5654',
      contactPreference: 'email',
      dateOfBirth: new Date('08/06/1974'),
      department: 2,
      salary: 42350.00,
      isActive: true,
      imageUrl: 'assets/images/female_01.jpg'
    },
    {
      employeeId: "3",
      firstName: 'Kevin',
      middleInitial: '',
      lastName: 'Bingamin',
      email: 'kabingamin12864@gmail.com',
      phoneNumber: '980-745-5654',
      contactPreference: 'email',
      dateOfBirth: new Date('12/08/1964'),
      department: 5,
      salary: 81587.65,
      isActive: true,
      imageUrl: 'assets/images/male_02.jpg'
    },
    {
      employeeId: "4",
      firstName: 'Paul',
      middleInitial: 'R',
      lastName: 'Clark',
      email: 'prclark122264@gmail.com',
      phoneNumber: '980-745-5654',
      contactPreference: 'phone',
      dateOfBirth: new Date('12/22/1964'),
      department: 4,
      salary: 105000.00,
      isActive: true,
      imageUrl: 'assets/images/male_03.jpg'
    }
  ];

  private listOfDepartments: IDepartment[] = [
    {id: 1, name: 'Help Desk'},
    {id: 2, name: 'Information Technology'},
    {id: 3, name: 'Human Resources'},
    {id: 4, name: 'Special Operations'}
  ]

  getEmployees(): IEmployee[] {
    return this.listOfEmployees;
  }

  getDepartments(): IDepartment[] {
    return this.listOfDepartments;
  }
  
  save(employee: IEmployee){
    this.listOfEmployees.push(employee);
  }
}
