import { Component, OnInit, OnChanges, OnDestroy, SimpleChanges, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import { IEmployee } from '../interfaces/employee';
import { EmployeeService } from '../services/employee.service';
import { EmployeeClearancePipe } from '../pipes/employeeClearance.pipe';
//import { DisplayEmployeeCardViewComponent } from '../list-employees-card-view/list-employee-card-view.component';
// import { DisplayEmployeeTableViewComponent } from '../list-employees-table-view/display-employee-table-view.component';

import { EmployeeCardViewComponent } from '../employee-card-view/employee-card-view.component';
import { EmployeeTableViewComponent } from '../employee-table-view/employee-table-view.component';


@Component({
  standalone: true,
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css'],
  imports: [CommonModule, EmployeeClearancePipe, EmployeeCardViewComponent, EmployeeTableViewComponent]
})
export class ListEmployeesComponent implements OnInit, OnChanges, OnDestroy{
  @Input()
  ngSwitchDefault: any;
  tableView: boolean = true;
  employees!: IEmployee[];

  constructor (private _employeeService: EmployeeService) {
    console.log("The list-employees component constructor has been called!")
  }

  ngOnInit(): void {
    this.employees = this._employeeService.getEmployees();
  }

  ngOnChanges(): void {}

  ngOnDestroy(): void {}

  toggleView(): void{
    this.tableView = !this.tableView;
  }

  trackByEmpCode(index: number, employee: any): string{
    return employee.code;
  }
}
