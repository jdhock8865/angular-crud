import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IEmployee } from '../interfaces/employee';
import { EmployeeClearancePipe } from '../pipes/employeeClearance.pipe';

@Component({
  selector: 'app-employee-table-view',
  standalone: true,
  imports: [CommonModule, EmployeeClearancePipe],
  templateUrl: './employee-table-view.component.html',
  styleUrls: ['./employee-table-view.component.css']
})
export class EmployeeTableViewComponent {

  @Input()
  employees!: IEmployee[];

  ngOnInit(): void {
    console.log("Here is the employee data in the display-employee.component: " + this.employees);
  }

  trackByEmpCode(index: number, employee: any): string{
    return employee.code;
  }

}
