import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    standalone: true,
    name: 'employeeClearance'
})
export class EmployeeClearancePipe implements PipeTransform{
    transform(value:string | null, department: number | null){
        if(department! > 3){
            return value + "-TS"
        } else {
            return value + "";
        }
    }
}