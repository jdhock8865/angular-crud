export interface IEmployee {
    employeeId: string | null;
    firstName: string | null;
    middleInitial: string | null;
    lastName: string | null;
    email: string  | null;
    phoneNumber: string | null;
    contactPreference: string | null;
    dateOfBirth: Date | null;
    department: number | null;
    salary: number | null;
    isActive: boolean | null;
    imageUrl: string | null;
}