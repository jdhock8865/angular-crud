import { Component, OnInit, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { NgForm, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IEmployee } from '../interfaces/employee';
import { IDepartment } from '../interfaces/department';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';

@Component({
  standalone: true,
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css'],
  imports: [FormsModule, CommonModule]
})
export class CreateEmployeeComponent implements OnInit, OnChanges, OnDestroy{
  
  departments!: IDepartment[];
  newEmployee = <IEmployee>{};
  
  applyBoldClass: boolean = true;
  applyItalicsClass: boolean = true;
  applyColorClass: boolean = true;

  isDisabled: boolean = this.disableSaveButton();
  showFormValidationProperties: boolean = false;
  showSalary: boolean = true;

  constructor (private _employeeService: EmployeeService, private _router: Router) {
    console.log("The screate-employee component constructor has been called!")
  }

  ngOnInit(): void {
    this.departments = this._employeeService.getDepartments();
    this.initializeEmployee();
  }

  ngOnChanges(): void {}

  ngOnDestroy(): void {}

  disableSaveButton() :boolean {
    return false;
  }

  addCSSClasses() {
    let classes = {
        boldClass: this.applyBoldClass,
        italicsClass: this.applyItalicsClass,
        colorClass: this.applyColorClass
    }
    return classes;
  }

  onClick(): void{
    console.log("Save Button Employee")
  }

  unhideFormValidationProperties(): boolean{
    return !this.showFormValidationProperties;
  }

  initializeEmployee(): void {
    this.newEmployee.employeeId = null;
    this.newEmployee.firstName = null;
    this.newEmployee.middleInitial = null;
    this.newEmployee.lastName = null;
    this.newEmployee.email = null;
    this.newEmployee.phoneNumber = null;
    this.newEmployee.contactPreference = null;
    this.newEmployee.dateOfBirth = null;
    this.newEmployee.department = -1;
    this.newEmployee.salary = null;
    this.newEmployee.isActive = null;
    this.newEmployee.imageUrl = null;
  }

  saveEmployee(employeeForm: NgForm)  :void {
    this.newEmployee.employeeId = employeeForm.value.employeeId;
    this.newEmployee.firstName = employeeForm.value.firstName;
    this.newEmployee.middleInitial = employeeForm.value.middleInitial;
    this.newEmployee.lastName = employeeForm.value.lastName;
    this.newEmployee.email = employeeForm.value.email;
    this.newEmployee.phoneNumber = employeeForm.value.phoneNumber;
    this.newEmployee.contactPreference = employeeForm.value.contactPreference;
    this.newEmployee.dateOfBirth = employeeForm.value.dateOfBirth;
    this.newEmployee.department = parseInt(employeeForm.value.department);
    this.newEmployee.salary = parseFloat(employeeForm.value.salary);
    this.newEmployee.isActive = employeeForm.value.isActive === true ? true : false;
    this.newEmployee.imageUrl = employeeForm.value.imageUrl;

    console.log(this.newEmployee);
    console.log(employeeForm.value);


    this._employeeService.save(this.newEmployee);

    this._router.navigate(['employees']);
    
  }
}
