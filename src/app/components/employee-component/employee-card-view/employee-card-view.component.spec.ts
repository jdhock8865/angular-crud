import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeCardViewComponent } from './employee-card-view.component';

describe('EmployeeCardViewComponent', () => {
  let component: EmployeeCardViewComponent;
  let fixture: ComponentFixture<EmployeeCardViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ EmployeeCardViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
