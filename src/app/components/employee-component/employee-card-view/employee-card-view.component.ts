import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IEmployee } from '../interfaces/employee';
import { EmployeeClearancePipe } from '../pipes/employeeClearance.pipe';

@Component({
  selector: 'app-employee-card-view',
  standalone: true,
  imports: [CommonModule, EmployeeClearancePipe],
  templateUrl: './employee-card-view.component.html',
  styleUrls: ['./employee-card-view.component.css']
})
export class EmployeeCardViewComponent implements OnInit {

  @Input()
  employee!: IEmployee;

  ngOnInit(): void {
    console.log("Here is the employee data in the employee-card-view.component: " + this.employee);
  }

}
