import { Component, OnInit, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ShowElementStateComponent } from '../show-element-state/show-element-state.component';
import { ShowElementGroupingComponent } from '../show-element-grouping/show-element-grouping.component';
import { IElement } from '../interfaces/element';
import { ElementService } from '../services/element.service';
import { ElementCardViewComponent } from '../element-card-view/element-card-view.component';
import { ElementTableViewComponent } from '../element-table-view/element-table-view.component';

@Component({
  standalone: true,
  templateUrl: './list-elements.component.html',
  styleUrls: ['./list-elements.component.css'],
  imports: [CommonModule,
    RouterModule,
    ShowElementStateComponent,
    ShowElementGroupingComponent,
    ElementCardViewComponent,
    ElementTableViewComponent]
})
export class ListElementsComponent implements OnInit, OnChanges, OnDestroy {
  elements!: IElement[];
  tableView: boolean = false;
  countOfLiquidElements!: number;
  groupingCount!: number;
  groupingArray!: number[] | any;

  subscribeCallbackObjects: any ={
    next: (elements: IElement[]) => this.elements = elements,
    error: (err: any) => {
      console.log("Oh fuck! We have an error!");
      console.log(err);
    },
    complete: () => {
      //console.log("Here is the list of elements:");
      //console.log(this.elements);
  }}

  selectedElementCountRadioButton: string = 'All';
  selectedElementGroupDropdown: any = "All Groups";
  statusMessage: string = 'Loading data. Please wait...'
  
  constructor (private _elementService: ElementService) {
    //console.log("The list-elements component constructor has been called!")
  }

  ngOnInit(): void {  
    this._elementService.getAllElements().subscribe(this.subscribeCallbackObjects);  
    // this.elements = this._elementService.getAllElements();
  }

  ngOnChanges(): void {}

  ngOnDestroy(): void {}

  toggleView(): void{
    this.tableView = !this.tableView;
  }

  trackByElementCode(index: number, element: any): string{
    return element.code;
  }
  
  getTotalElementsCount(): number{
    return this.elements.length;
  }

  getCountOfSolidElements(): number{
    return this.elements.filter(e => e.element_state === "solid").length
  }

  getCountOfGasElements(): number{
    return this.elements.filter(e => e.element_state === "gas").length
  }

  getCountOfLiquidElements(): number{
    this.countOfLiquidElements = this.elements.filter(e => e.element_state === "liquid").length;
    console.log("Number of elements in a liquid state: " + this.countOfLiquidElements);
    return this.elements.filter(e => e.element_state === "liquid").length
  }

  onElementCountRadioButtonChange(selectedRadioButtonValue: string): void{
    this.selectedElementCountRadioButton = selectedRadioButtonValue;
  }

  getElementGroupings(): number[] {
    
    this.groupingArray = this.elements.map(e => e.element_group);

    let filteredArray = this.groupingArray.filter((item: any, pos: any) =>{
      return this.groupingArray.indexOf(item) == pos;
    })

    let sortedArray = filteredArray.sort((a: any, b: any) => a - b);
    //console.log("Here are all of the unique values in the element_group column sorted: " + sortedArray);
    return sortedArray;
  }

  onElementGroupDropDownChange(selectedDropDownValue: number): void {
    this.selectedElementGroupDropdown = selectedDropDownValue;
  }

  openModal(){
    alert("I am trying to open a modal");
  }
}
