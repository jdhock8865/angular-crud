import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowElementGroupingComponent } from './show-element-grouping.component';

describe('ShowElementGroupingComponent', () => {
  let component: ShowElementGroupingComponent;
  let fixture: ComponentFixture<ShowElementGroupingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ ShowElementGroupingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowElementGroupingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
