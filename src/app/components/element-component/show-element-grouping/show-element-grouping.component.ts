import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


@Component({
  standalone: true,
  selector: 'app-show-element-grouping',
  templateUrl: './show-element-grouping.component.html',
  styleUrls: ['./show-element-grouping.component.css'],
  imports: [CommonModule, FormsModule],
})
export class ShowElementGroupingComponent implements OnInit, OnChanges, OnDestroy {
  selectedDropDownValue: any = "All Groups";

  @Input()
  optionsList!: number[];

  @Output()
  valueDropDownUpdated: EventEmitter<number> = new EventEmitter<number>();

  ngOnInit(): void {}

  ngOnChanges(): void {}

  ngOnDestroy(): void {}

  onDropdownSelection(){
    this.valueDropDownUpdated.emit(this.selectedDropDownValue);
    console.log("selected group " + this.selectedDropDownValue);
  }
}
