import { Component, OnInit, OnChanges, OnDestroy, } from '@angular/core';
import { NgForm, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IElement } from '../interfaces/element';

@Component({
  standalone: true,
  templateUrl: './create-element.component.html',
  styleUrls: ['./create-element.component.css'],
  imports: [FormsModule, CommonModule]
})
export class CreateElementComponent implements OnInit, OnChanges, OnDestroy{
  
  newElement = <IElement>{};

  applyBoldClass: boolean = true;
  applyItalicsClass: boolean = true;
  applyColorClass: boolean = true;

  isDisabled: boolean = this.disableSaveButton();
  showFormValidationProperties: boolean = true;

  constructor () {
    console.log("The create element component constructor has been called!");
  }

  ngOnInit() {
    this.initializeElement();
  }

  ngOnChanges(): void {}

  ngOnDestroy(): void {}

  disableSaveButton() :boolean {
    return false;
  }

  addCSSClasses() {
    let classes = {
        boldClass: this.applyBoldClass,
        italicsClass: this.applyItalicsClass,
        colorClass: this.applyColorClass
    }
    return classes;
  }

  onClick(): void{
    console.log("Saving New Element")
  }

  unhideFormValidationProperties(): boolean{
    return !this.showFormValidationProperties;
  }

  initializeElement(): void {
    this.newElement.atomic_number = null;
    this.newElement.atomic_weight = null;
    this.newElement.element_name = null;
    this.newElement.element_symbol = null;
    this.newElement.element_state = null;
    this.newElement.element_group = null;
    this.newElement.element_discovery = null;
    this.newElement.element_image = null;
  }

  saveElement(elementForm: NgForm)  :void {
    this.newElement.atomic_number = parseInt(elementForm.value.elementNumber);
    this.newElement.atomic_weight = parseFloat(elementForm.value.elementWeight);
    this.newElement.element_name = elementForm.value.elementName; // need to ensure first letter is capitalized
    this.newElement.element_symbol = elementForm.value.elementSymbol;
    this.newElement.element_state = elementForm.value.elementState;
    this.newElement.element_group = parseInt(elementForm.value.elementGroup);
    this.newElement.element_discovery = elementForm.value.elementDiscovery;
    this.newElement.element_image = 'assets/images/' + elementForm.value.elementName + '.pmg';

    console.log(this.newElement);
    console.log(elementForm.value);
  }
}
