import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { IElement } from '../interfaces/element';
import { ElementService } from '../services/element.service';

@Component({
  standalone: true,
  selector: 'app-element-details',
  templateUrl: './element-details.component.html',
  styleUrls: ['./element-details.component.css'],
  imports:[CommonModule, RouterModule]
})
export class ElementDetailsComponent implements OnInit{
  elements!: IElement[];
  statusMessage!: string;

  subscribeCallbackObjects: any ={
    next: (elements: IElement[]) => this.elements = elements,
    error: (err: any) => console.log(err),
    complete: () => console.log(this.elements)}

  constructor(private _elementService: ElementService, private _activatedRoute: ActivatedRoute, private _router: Router){}

  ngOnInit(): void {
    console.log('ngOnitInit in element-details firing!')
    let atomic_number: string = this._activatedRoute.snapshot.params['atomic_number'];
   
    this._elementService.getElementByAtomicNumber(atomic_number).subscribe(this.subscribeCallbackObjects);
  }

  returnToElementList(): void{
    this._router.navigate(['/elements'])
  }

}
