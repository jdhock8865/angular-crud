import { Injectable } from '@angular/core';
import { IElement } from '../interfaces/element';
import { IElementState } from '../interfaces/elementState';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ElementService {

  private readonly API_URL = 'http://localhost:9005/api/elements';
  
  constructor(private _httpClient: HttpClient) { 
    //console.log("A new instance of the get elements service has been created.")
  }

  private listOfElements: IElement[] = [
    {
        "atomic_number": 1,
        "atomic_weight": 1.008,
        "element_name": "Hydrogen",
        "element_symbol": "H",
        "element_state": "gas",
        "element_group": 1,
        "element_discovery": "2023",
        "element_image": "assets/images/hydrogen.png"
    },
    {
        "atomic_number": 2,
        "atomic_weight": 4.003,
        "element_name": "Helium",
        "element_symbol": "He",
        "element_state": "gas",
        "element_group": 18,
        "element_discovery": "2023",
        "element_image": "assets/images/helium.png",
    },
    {
        "atomic_number": 3,
        "atomic_weight": 6.941,
        "element_name": "Lithium",
        "element_symbol": "Li",
        "element_state": "solid",
        "element_group": 1,
        "element_discovery": "2023",
        "element_image": "assets/images/lithium.png"
    },
    {
        "atomic_number": 4,
        "atomic_weight": 9.012,
        "element_name": "Beryllium",
        "element_symbol": "Be",
        "element_state": "solid",
        "element_group": 2,
        "element_discovery": "2023",
        "element_image": "assets/images/beryllium.png"
    },
    {
        "atomic_number": 5,
        "atomic_weight": 10.811,
        "element_name": "Boron",
        "element_symbol": "B",
        "element_state": "solid",
        "element_group": 13,
        "element_discovery": "2023",
        "element_image": "assets/images/boron.png"
    },
    {
        "atomic_number": 6,
        "atomic_weight": 12.011,
        "element_name": "Carbon",
        "element_symbol": "C",
        "element_state": "solid",
        "element_group": 14,
        "element_discovery": "2023",
        "element_image": "assets/images/carbon.png"
    },
    {
        "atomic_number": 7,
        "atomic_weight": 14.007,
        "element_name": "Nitrogen",
        "element_symbol": "N",
        "element_state": "gas",
        "element_group": 15,
        "element_discovery": "2023",
        "element_image": "assets/images/nitrogen.png"
    },
    {
        "atomic_number": 8,
        "atomic_weight": 15.999,
        "element_name": "Oxygen",
        "element_symbol": "O",
        "element_state": "gas",
        "element_group": 16,
        "element_discovery": "2023",
        "element_image": "assets/images/oxygen.png"
    },
    {
        "atomic_number": 9,
        "atomic_weight": 18.998,
        "element_name": "Fluorine",
        "element_symbol": "F",
        "element_state": "gas",
        "element_group": 17,
        "element_discovery": "2023",
        "element_image": "assets/images/fluorine.png"
    },
    {
        "atomic_number": 10,
        "atomic_weight": 20.180,
        "element_name": "Neon",
        "element_symbol": "Ne",
        "element_state": "gas",
        "element_group": 18,
        "element_discovery": "2023",
        "element_image": "assets/images/neon.png"
    },
    {
        "atomic_number": 11,
        "atomic_weight": 22.990,
        "element_name": "Sodium",
        "element_symbol": "Na",
        "element_state": "solid",
        "element_group": 1,
        "element_discovery": "2023",
        "element_image": "assets/images/sodium.png"
    },
    {
        "atomic_number": 12,
        "atomic_weight": 24.305,
        "element_name": "Magnesium",
        "element_symbol": "Mg",
        "element_state": "solid",
        "element_group": 2,
        "element_discovery": "2023",
        "element_image": "assets/images/magnesium.png"
    },
    {
        "atomic_number": 13,
        "atomic_weight": 26.982,
        "element_name": "Aluminum",
        "element_symbol": "Al",
        "element_state": "solid",
        "element_group": 13,
        "element_discovery": "2023",
        "element_image": "assets/images/aluminum.png"
    },
    {
        "atomic_number": 14,
        "atomic_weight": 28.086,
        "element_name": "Silicon",
        "element_symbol": "Si",
        "element_state": "solid",
        "element_group": 14,
        "element_discovery": "2023",
        "element_image": "assets/images/silicon.png"
    },
    {
        "atomic_number": 15,
        "atomic_weight": 30.974,
        "element_name": "Phosphorus",
        "element_symbol": "P",
        "element_state": "solid",
        "element_group": 15,
        "element_discovery": "2023",
        "element_image": "assets/images/phosphorus.png"
    },
    {
        "atomic_number": 16,
        "atomic_weight": 32.065,
        "element_name": "Sulfur",
        "element_symbol": "S",
        "element_state": "solid",
        "element_group": 16,
        "element_discovery": "2023",
        "element_image": "assets/images/sulfur.png"
    },
    {
        "atomic_number": 17,
        "atomic_weight": 35.453,
        "element_name": "Chlorine",
        "element_symbol": "Cl",
        "element_state": "gas",
        "element_group": 17,
        "element_discovery": "2023",
        "element_image": "assets/images/chlorine.png"
    },
    {
        "atomic_number": 18,
        "atomic_weight": 39.948,
        "element_name": "Argon",
        "element_symbol": "Ar",
        "element_state": "gas",
        "element_group": 18,
        "element_discovery": "2023",
        "element_image": "assets/images/argon.png"
    },
    {
        "atomic_number": 19,
        "atomic_weight": 39.098,
        "element_name": "Potassium",
        "element_symbol": "K",
        "element_state": "solid",
        "element_group": 1,
        "element_discovery": "2023",
        "element_image": "assets/images/potassium.png"
    },
    {
        "atomic_number": 20,
        "atomic_weight": 40.078,
        "element_name": "Calcium",
        "element_symbol": "Ca",
        "element_state": "solid",
        "element_group": 2,
        "element_discovery": "2023",
        "element_image": "assets/images/calcium.png"
    },
    {
        "atomic_number": 21,
        "atomic_weight": 44.956,
        "element_name": "Scandium",
        "element_symbol": "Sc",
        "element_state": "solid",
        "element_group": 3,
        "element_discovery": "2023",
        "element_image": "assets/images/scandium.png"
    },
    {
        "atomic_number": 22,
        "atomic_weight": 47.867,
        "element_name": "Titanium",
        "element_symbol": "Ti",
        "element_state": "solid",
        "element_group": 4,
        "element_discovery": "2023",
        "element_image": "assets/images/titanium.png"
    },
    {
        "atomic_number": 23,
        "atomic_weight": 50.942,
        "element_name": "Vanadium",
        "element_symbol": "V",
        "element_state": "solid",
        "element_group": 5,
        "element_discovery": "2023",
        "element_image": "assets/images/vanadium.png"
    },
    {
        "atomic_number": 24,
        "atomic_weight": 51.996,
        "element_name": "Chromium",
        "element_symbol": "Cr",
        "element_state": "solid",
        "element_group": 6,
        "element_discovery": "2023",
        "element_image": "assets/images/chromium.png"
    },
    {
        "atomic_number": 25,
        "atomic_weight": 54.938,
        "element_name": "Manganese",
        "element_symbol": "Mn",
        "element_state": "solid",
        "element_group": 7,
        "element_discovery": "2023",
        "element_image": "assets/images/manganese.png"
    },
    {
        "atomic_number": 26,
        "atomic_weight": 55.845,
        "element_name": "Iron",
        "element_symbol": "Fe",
        "element_state": "solid",
        "element_group": 8,
        "element_discovery": "2023",
        "element_image": "assets/images/iron.png"
    },
    {
        "atomic_number": 27,
        "atomic_weight": 58.933,
        "element_name": "Cobalt",
        "element_symbol": "Co",
        "element_state": "solid",
        "element_group": 9,
        "element_discovery": "2023",
        "element_image": "assets/images/cobalt.png"
    },
    {
        "atomic_number": 28,
        "atomic_weight": 58.693,
        "element_name": "Nickel",
        "element_symbol": "Ni",
        "element_state": "solid",
        "element_group": 10,
        "element_discovery": "2023",
        "element_image": "assets/images/nickel.png"
    },
    {
        "atomic_number": 29,
        "atomic_weight": 63.546,
        "element_name": "Copper",
        "element_symbol": "Cu",
        "element_state": "solid",
        "element_group": 11,
        "element_discovery": "2023",
        "element_image": "assets/images/copper.png"
    },
    {
        "atomic_number": 30,
        "atomic_weight": 65.390,
        "element_name": "Zinc",
        "element_symbol": "Zn",
        "element_state": "solid",
        "element_group": 12,
        "element_discovery": "2023",
        "element_image": "assets/images/zinc.png"
    },
    {
        "atomic_number": 31,
        "atomic_weight": 69.723,
        "element_name": "Gallium",
        "element_symbol": "Ga",
        "element_state": "solid",
        "element_group": 13,
        "element_discovery": "2023",
        "element_image": "assets/images/gallium.png"
    },
    {
        "atomic_number": 32,
        "atomic_weight": 72.640,
        "element_name": "Germanium",
        "element_symbol": "Ge",
        "element_state": "solid",
        "element_group": 14,
        "element_discovery": "2023",
        "element_image": "assets/images/germanium.png"
    },
    {
        "atomic_number": 33,
        "atomic_weight": 74.922,
        "element_name": "Arsenic",
        "element_symbol": "As",
        "element_state": "solid",
        "element_group": 15,
        "element_discovery": "2023",
        "element_image": "assets/images/arsenic.png"
    },
    {
        "atomic_number": 34,
        "atomic_weight": 78.960,
        "element_name": "Selenium",
        "element_symbol": "Se",
        "element_state": "solid",
        "element_group": 16,
        "element_discovery": "2023",
        "element_image": "assets/images/selenium.png"
    },
    {
        "atomic_number": 35,
        "atomic_weight": 79.904,
        "element_name": "Bromine",
        "element_symbol": "Br",
        "element_state": "liquid",
        "element_group": 17,
        "element_discovery": "2023",
        "element_image": "assets/images/bromine.png"
    },
    {
        "atomic_number": 36,
        "atomic_weight": 83.800,
        "element_name": "Krypton",
        "element_symbol": "Kr",
        "element_state": "gas",
        "element_group": 18,
        "element_discovery": "2023",
        "element_image": "assets/images/krypton.png"
    },
    {
        "atomic_number": 37,
        "atomic_weight": 85.468,
        "element_name": "Rubidium",
        "element_symbol": "Rb",
        "element_state": "solid",
        "element_group": 1,
        "element_discovery": "2023",
        "element_image": "assets/images/rubidium.png"
    },
    {
        "atomic_number": 38,
        "atomic_weight": 87.620,
        "element_name": "Strontium",
        "element_symbol": "Sr",
        "element_state": "solid",
        "element_group": 2,
        "element_discovery": "2023",
        "element_image": "assets/images/strontium.png"
    },
    {
        "atomic_number": 39,
        "atomic_weight": 88.906,
        "element_name": "Yttrium",
        "element_symbol": "Y",
        "element_state": "solid",
        "element_group": 3,
        "element_discovery": "2023",
        "element_image": "assets/images/yttrium.png"
    },
    {
        "atomic_number": 40,
        "atomic_weight": 91.224,
        "element_name": "Zirconium",
        "element_symbol": "Zr",
        "element_state": "solid",
        "element_group": 4,
        "element_discovery": "2023",
        "element_image": "assets/images/zirconium.png"
    },
    {
        "atomic_number": 41,
        "atomic_weight": 92.906,
        "element_name": "Niobium",
        "element_symbol": "Nb",
        "element_state": "solid",
        "element_group": 5,
        "element_discovery": "2023",
        "element_image": "assets/images/niobium.png"
    },
    {
        "atomic_number": 42,
        "atomic_weight": 95.940,
        "element_name": "Molybdenum",
        "element_symbol": "Mo",
        "element_state": "solid",
        "element_group": 6,
        "element_discovery": "2023",
        "element_image": "assets/images/molybdenum.png"
    },
    {
        "atomic_number": 43,
        "atomic_weight": 98.000,
        "element_name": "Technetium",
        "element_symbol": "Tc",
        "element_state": "solid",
        "element_group": 7,
        "element_discovery": "2023",
        "element_image": "assets/images/technetium.png"
    },
    {
        "atomic_number": 44,
        "atomic_weight": 101.070,
        "element_name": "Ruthenium",
        "element_symbol": "Ru",
        "element_state": "solid",
        "element_group": 8,
        "element_discovery": "2023",
        "element_image": "assets/images/ruthenium.png"
    },
    {
        "atomic_number": 45,
        "atomic_weight": 102.906,
        "element_name": "Rhodium",
        "element_symbol": "Rh",
        "element_state": "solid",
        "element_group": 9,
        "element_discovery": "2023",
        "element_image": "assets/images/rhodium.png"
    },
    {
        "atomic_number": 46,
        "atomic_weight": 106.420,
        "element_name": "Palladium",
        "element_symbol": "Pd",
        "element_state": "solid",
        "element_group": 10,
        "element_discovery": "2023",
        "element_image": "assets/images/palladium.png"
    },
    {
        "atomic_number": 47,
        "atomic_weight": 107.868,
        "element_name": "Silver",
        "element_symbol": "Ag",
        "element_state": "solid",
        "element_group": 11,
        "element_discovery": "2023",
        "element_image": "assets/images/silver.png"
    },
    {
        "atomic_number": 48,
        "atomic_weight": 112.411,
        "element_name": "Cadmium",
        "element_symbol": "Cd",
        "element_state": "solid",
        "element_group": 12,
        "element_discovery": "2023",
        "element_image": "assets/images/cadmium.png"
    },
    {
        "atomic_number": 49,
        "atomic_weight": 114.818,
        "element_name": "Indium",
        "element_symbol": "In",
        "element_state": "solid",
        "element_group": 13,
        "element_discovery": "2023",
        "element_image": "assets/images/indium.png"
    },
    {
        "atomic_number": 50,
        "atomic_weight": 118.710,
        "element_name": "Tin",
        "element_symbol": "Sn",
        "element_state": "solid",
        "element_group": 14,
        "element_discovery": "2023",
        "element_image": "assets/images/tin.png"
    },
    {
        "atomic_number": 51,
        "atomic_weight": 121.760,
        "element_name": "Antimony",
        "element_symbol": "Sb",
        "element_state": "solid",
        "element_group": 15,
        "element_discovery": "2023",
        "element_image": "assets/images/antimony.png"
    },
    {
        "atomic_number": 52,
        "atomic_weight": 127.600,
        "element_name": "Tellurium",
        "element_symbol": "Te",
        "element_state": "solid",
        "element_group": 16,
        "element_discovery": "2023",
        "element_image": "assets/images/tellurium.png"
    },
    {
        "atomic_number": 53,
        "atomic_weight": 126.905,
        "element_name": "Iodine",
        "element_symbol": "I",
        "element_state": "solid",
        "element_group": 17,
        "element_discovery": "2023",
        "element_image": "assets/images/iodine.png"
    },
    {
        "atomic_number": 54,
        "atomic_weight": 131.293,
        "element_name": "Xenon",
        "element_symbol": "Xe",
        "element_state": "gas",
        "element_group": 18,
        "element_discovery": "2023",
        "element_image": "assets/images/xenon.png"
    },
    {
        "atomic_number": 55,
        "atomic_weight": 132.906,
        "element_name": "Cesium",
        "element_symbol": "Cs",
        "element_state": "solid",
        "element_group": 1,
        "element_discovery": "2023",
        "element_image": "assets/images/cesium.png"
    },
    {
        "atomic_number": 56,
        "atomic_weight": 137.327,
        "element_name": "Barium",
        "element_symbol": "Ba",
        "element_state": "solid",
        "element_group": 2,
        "element_discovery": "2023",
        "element_image": "assets/images/barium.png"
    },
    {
        "atomic_number": 57,
        "atomic_weight": 138.906,
        "element_name": "Lanthanum",
        "element_symbol": "La",
        "element_state": "solid",
        "element_group": 3,
        "element_discovery": "2023",
        "element_image": "assets/images/lanthanum.png"
    },
    {
        "atomic_number": 58,
        "atomic_weight": 140.116,
        "element_name": "Cerium",
        "element_symbol": "Ce",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/cerium.png"
    },
    {
        "atomic_number": 59,
        "atomic_weight": 140.908,
        "element_name": "Praseodymium",
        "element_symbol": "Pr",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/praseodymium.png"
    },
    {
        "atomic_number": 60,
        "atomic_weight": 144.240,
        "element_name": "Neodymium",
        "element_symbol": "Nd",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/neodymium.png"
    },
    {
        "atomic_number": 61,
        "atomic_weight": 145.000,
        "element_name": "Promethium",
        "element_symbol": "Pm",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/promethium.png"
    },
    {
        "atomic_number": 62,
        "atomic_weight": 150.360,
        "element_name": "Samarium",
        "element_symbol": "Sm",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/samarium.png"
    },
    {
        "atomic_number": 63,
        "atomic_weight": 151.964,
        "element_name": "Europium",
        "element_symbol": "Eu",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/europium.png"
    },
    {
        "atomic_number": 64,
        "atomic_weight": 157.250,
        "element_name": "Gadolinium",
        "element_symbol": "Gd",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/gadolinium.png"
    },
    {
        "atomic_number": 65,
        "atomic_weight": 158.925,
        "element_name": "Terbium",
        "element_symbol": "Tb",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/terbium.png"
    },
    {
        "atomic_number": 66,
        "atomic_weight": 162.500,
        "element_name": "Dysprosium",
        "element_symbol": "Dy",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/dysprosium.png"
    },
    {
        "atomic_number": 67,
        "atomic_weight": 164.930,
        "element_name": "Holmium",
        "element_symbol": "Ho",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/holmium.png"
    },
    {
        "atomic_number": 68,
        "atomic_weight": 167.259,
        "element_name": "Erbium",
        "element_symbol": "Er",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/erbium.png"
    },
    {
        "atomic_number": 69,
        "atomic_weight": 168.934,
        "element_name": "Thulium",
        "element_symbol": "Tm",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/thulium.png"
    },
    {
        "atomic_number": 70,
        "atomic_weight": 173.040,
        "element_name": "Ytterbium",
        "element_symbol": "Yb",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/ytterbium.png"
    },
    {
        "atomic_number": 71,
        "atomic_weight": 174.967,
        "element_name": "Lutetium",
        "element_symbol": "Lu",
        "element_state": "solid",
        "element_group": 101,
        "element_discovery": "2023",
        "element_image": "assets/images/lutetium.png"
    },
    {
        "atomic_number": 72,
        "atomic_weight": 178.490,
        "element_name": "Hafnium",
        "element_symbol": "Hf",
        "element_state": "solid",
        "element_group": 4,
        "element_discovery": "2023",
        "element_image": "assets/images/hafnium.png"
    },
    {
        "atomic_number": 73,
        "atomic_weight": 180.948,
        "element_name": "Tantalum",
        "element_symbol": "Ta",
        "element_state": "solid",
        "element_group": 5,
        "element_discovery": "2023",
        "element_image": "assets/images/tantalum.png"
    },
    {
        "atomic_number": 74,
        "atomic_weight": 183.840,
        "element_name": "Tungsten",
        "element_symbol": "W",
        "element_state": "solid",
        "element_group": 6,
        "element_discovery": "2023",
        "element_image": "assets/images/tungsten.png"
    },
    {
        "atomic_number": 75,
        "atomic_weight": 186.207,
        "element_name": "Rhenium",
        "element_symbol": "Re",
        "element_state": "solid",
        "element_group": 7,
        "element_discovery": "2023",
        "element_image": "assets/images/rhenium.png"
    },
    {
        "atomic_number": 76,
        "atomic_weight": 190.230,
        "element_name": "Osmium",
        "element_symbol": "Os",
        "element_state": "solid",
        "element_group": 8,
        "element_discovery": "2023",
        "element_image": "assets/images/osmium.png"
    },
    {
        "atomic_number": 77,
        "atomic_weight": 192.217,
        "element_name": "Iridium",
        "element_symbol": "Ir",
        "element_state": "solid",
        "element_group": 9,
        "element_discovery": "2023",
        "element_image": "assets/images/iridium.png"
    },
    {
        "atomic_number": 78,
        "atomic_weight": 195.078,
        "element_name": "Platinum",
        "element_symbol": "Pt",
        "element_state": "solid",
        "element_group": 10,
        "element_discovery": "2023",
        "element_image": "assets/images/platinum.png"
    },
    {
        "atomic_number": 79,
        "atomic_weight": 196.967,
        "element_name": "Gold",
        "element_symbol": "Au",
        "element_state": "solid",
        "element_group": 11,
        "element_discovery": "2023",
        "element_image": "assets/images/gold.png"
    },
    {
        "atomic_number": 80,
        "atomic_weight": 200.590,
        "element_name": "Mercury",
        "element_symbol": "Hg",
        "element_state": "liquid",
        "element_group": 12,
        "element_discovery": "2023",
        "element_image": "assets/images/mercury.png"
    },
    {
        "atomic_number": 81,
        "atomic_weight": 204.383,
        "element_name": "Thallium",
        "element_symbol": "Tl",
        "element_state": "solid",
        "element_group": 13,
        "element_discovery": "2023",
        "element_image": "assets/images/thallium.png"
    },
    {
        "atomic_number": 82,
        "atomic_weight": 207.200,
        "element_name": "Lead",
        "element_symbol": "Pb",
        "element_state": "solid",
        "element_group": 14,
        "element_discovery": "2023",
        "element_image": "assets/images/lead.png"
    },
    {
        "atomic_number": 83,
        "atomic_weight": 208.980,
        "element_name": "Bismuth",
        "element_symbol": "Bi",
        "element_state": "solid",
        "element_group": 15,
        "element_discovery": "2023",
        "element_image": "assets/images/bismuth.png"
    },
    {
        "atomic_number": 84,
        "atomic_weight": 209.000,
        "element_name": "Polonium",
        "element_symbol": "Po",
        "element_state": "solid",
        "element_group": 16,
        "element_discovery": "2023",
        "element_image": "assets/images/polonium.png"
    },
    {
        "atomic_number": 85,
        "atomic_weight": 210.000,
        "element_name": "Astatine",
        "element_symbol": "At",
        "element_state": "solid",
        "element_group": 17,
        "element_discovery": "2023",
        "element_image": "assets/images/astatine.png"
    },
    {
        "atomic_number": 86,
        "atomic_weight": 222.000,
        "element_name": "Radon",
        "element_symbol": "Rn",
        "element_state": "gas",
        "element_group": 18,
        "element_discovery": "2023",
        "element_image": "assets/images/radon.png"
    },
    {
        "atomic_number": 87,
        "atomic_weight": 223.000,
        "element_name": "Francium",
        "element_symbol": "Fr",
        "element_state": "solid",
        "element_group": 1,
        "element_discovery": "2023",
        "element_image": "assets/images/francium.png"
    },
    {
        "atomic_number": 88,
        "atomic_weight": 226.000,
        "element_name": "Radium",
        "element_symbol": "Ra",
        "element_state": "solid",
        "element_group": 2,
        "element_discovery": "2023",
        "element_image": "assets/images/radium.png"
    },
    {
        "atomic_number": 89,
        "atomic_weight": 227.000,
        "element_name": "Actinium",
        "element_symbol": "Ac",
        "element_state": "solid",
        "element_group": 3,
        "element_discovery": "2023",
        "element_image": "assets/images/actinium.png"
    },
    {
        "atomic_number": 90,
        "atomic_weight": 232.038,
        "element_name": "Thorium",
        "element_symbol": "Th",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/thorium.png"
    },
    {
        "atomic_number": 91,
        "atomic_weight": 231.036,
        "element_name": "Protactinium",
        "element_symbol": "Pa",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/protactinium.png"
    },
    {
        "atomic_number": 92,
        "atomic_weight": 238.029,
        "element_name": "Uranium",
        "element_symbol": "U",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/uranium.png"
    },
    {
        "atomic_number": 93,
        "atomic_weight": 237.000,
        "element_name": "Neptunium",
        "element_symbol": "Np",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/neptunium.png"
    },
    {
        "atomic_number": 94,
        "atomic_weight": 244.000,
        "element_name": "Plutonium",
        "element_symbol": "Pu",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/plutonium.png"
    },
    {
        "atomic_number": 95,
        "atomic_weight": 243.000,
        "element_name": "Americium",
        "element_symbol": "Am",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/americium.png"
    },
    {
        "atomic_number": 96,
        "atomic_weight": 247.000,
        "element_name": "Curium",
        "element_symbol": "Cm",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/curium.png"
    },
    {
        "atomic_number": 97,
        "atomic_weight": 247.000,
        "element_name": "Berkelium",
        "element_symbol": "Bk",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/berkelium.png"
    },
    {
        "atomic_number": 98,
        "atomic_weight": 251.000,
        "element_name": "Californium",
        "element_symbol": "Cf",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/californium.png"
    },
    {
        "atomic_number": 99,
        "atomic_weight": 252.000,
        "element_name": "Einsteinium",
        "element_symbol": "Es",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/einsteinium.png"
    },
    {
        "atomic_number": 100,
        "atomic_weight": 257.000,
        "element_name": "Fermium",
        "element_symbol": "Fm",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/fermium.png"
    },
    {
        "atomic_number": 101,
        "atomic_weight": 258.000,
        "element_name": "Mendelevium",
        "element_symbol": "Md",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/mendelevium.png"
    },
    {
        "atomic_number": 102,
        "atomic_weight": 259.000,
        "element_name": "Nobelium",
        "element_symbol": "No",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/nobelium.png"
    },
    {
        "atomic_number": 103,
        "atomic_weight": 262.000,
        "element_name": "Lawrencium",
        "element_symbol": "Lr",
        "element_state": "solid",
        "element_group": 102,
        "element_discovery": "2023",
        "element_image": "assets/images/lawrencium.png"
    },
    {
        "atomic_number": 104,
        "atomic_weight": 261.000,
        "element_name": "Rutherfordium",
        "element_symbol": "Rf",
        "element_state": "solid",
        "element_group": 4,
        "element_discovery": "2023",
        "element_image": "assets/images/rutherfordium.png"
    },
    {
        "atomic_number": 105,
        "atomic_weight": 262.000,
        "element_name": "Dubnium",
        "element_symbol": "Db",
        "element_state": "solid",
        "element_group": 5,
        "element_discovery": "2023",
        "element_image": "assets/images/dubnium.png"
    },
    {
        "atomic_number": 106,
        "atomic_weight": 266.000,
        "element_name": "Seaborgium",
        "element_symbol": "Sg",
        "element_state": "solid",
        "element_group": 6,
        "element_discovery": "2023",
        "element_image": "assets/images/seaborgium.png"
    },
    {
        "atomic_number": 107,
        "atomic_weight": 264.000,
        "element_name": "Bohrium",
        "element_symbol": "Bh",
        "element_state": "solid",
        "element_group": 7,
        "element_discovery": "2023",
        "element_image": "assets/images/bohrium.png"
    },
    {
        "atomic_number": 108,
        "atomic_weight": 277.000,
        "element_name": "Hassium",
        "element_symbol": "Hs",
        "element_state": "solid",
        "element_group": 8,
        "element_discovery": "2023",
        "element_image": "assets/images/hassium.png"
    },
    {
        "atomic_number": 109,
        "atomic_weight": 268.000,
        "element_name": "Meitnerium",
        "element_symbol": "Mt",
        "element_state": "solid",
        "element_group": 9,
        "element_discovery": "2023",
        "element_image": "assets/images/meitnerium.png"
    },
    {
        "atomic_number": 110,
        "atomic_weight": 1.008,
        "element_name": "HydrogenPlus",
        "element_symbol": "H+",
        "element_state": "gas",
        "element_group": 1,
        "element_discovery": "2025",
        "element_image": "assets/images/hydrogen.png"
    }
  ]

//   getAllElements(): IElement[] {
//     return this.listOfElements;
//   }
  getAllElements(): Observable<IElement[]>{
    return this._httpClient.get<IElement[]>(this.API_URL + "/getElementList");
  }

  getElementByAtomicNumber(atomic_number: string): Observable<IElement[]> {
    console.log("The atomic number: " + atomic_number);
    return this._httpClient.get<IElement[]>(this.API_URL + "/findElement/" + atomic_number);
  }

  getElementStates(): Observable<IElementState[]>{
    return this._httpClient.get<IElementState[]>(this.API_URL + "/getElementStates");
  }
}
