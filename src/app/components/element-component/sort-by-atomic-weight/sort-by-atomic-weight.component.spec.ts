import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SortByAtomicWeightComponent } from './sort-by-atomic-weight.component';

describe('SortByAtomicWeightComponent', () => {
  let component: SortByAtomicWeightComponent;
  let fixture: ComponentFixture<SortByAtomicWeightComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ SortByAtomicWeightComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SortByAtomicWeightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
