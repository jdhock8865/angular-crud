import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-sort-by-atomic-weight',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sort-by-atomic-weight.component.html',
  styleUrls: ['./sort-by-atomic-weight.component.css']
})
export class SortByAtomicWeightComponent {

}
