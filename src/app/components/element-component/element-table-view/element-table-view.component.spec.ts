import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementTableViewComponent } from './element-table-view.component';

describe('ElementTableViewComponent', () => {
  let component: ElementTableViewComponent;
  let fixture: ComponentFixture<ElementTableViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ ElementTableViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ElementTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
