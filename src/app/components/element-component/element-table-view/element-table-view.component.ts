import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IElement } from '../interfaces/element';
import { ShowElementGroupingComponent } from '../show-element-grouping/show-element-grouping.component';
import { ShowElementStateComponent } from '../show-element-state/show-element-state.component';

@Component({
  selector: 'app-element-table-view',
  standalone: true,
  imports: [CommonModule, RouterModule, ShowElementGroupingComponent, ShowElementStateComponent],
  templateUrl: './element-table-view.component.html',
  styleUrls: ['./element-table-view.component.css']
})
export class ElementTableViewComponent {

  countOfLiquidElements!: number;
  groupingCount!: number;
  groupingArray!: number[] | any;
  selectedElementCountRadioButton: string = 'All';
  
  selectedElementGroupDropdown: any = "All Groups";
  statusMessage: string = 'Loading data. Please wait...'
  
  
  @Input()
  elements!: IElement[];
  
  getElementGroupings(): number[] {
    
    this.groupingArray = this.elements.map(e => e.element_group);

    let filteredArray = this.groupingArray.filter((item: any, pos: any) =>{
      return this.groupingArray.indexOf(item) == pos;
    })

    let sortedArray = filteredArray.sort((a: any, b: any) => a - b);
    //console.log("Here are all of the unique values in the element_group column sorted: " + sortedArray);
    return sortedArray;
  }

  onElementGroupDropDownChange(selectedDropDownValue: number): void {
    this.selectedElementGroupDropdown = selectedDropDownValue;
  }

  getTotalElementsCount(): number{
    return this.elements.length;
  }

  getCountOfSolidElements(): number{
    return this.elements.filter(e => e.element_state === "solid").length
  }

  getCountOfGasElements(): number{
    return this.elements.filter(e => e.element_state === "gas").length
  }

  getCountOfLiquidElements(): number{
    this.countOfLiquidElements = this.elements.filter(e => e.element_state === "liquid").length;
    console.log("Number of elements in a liquid state: " + this.countOfLiquidElements);
    return this.elements.filter(e => e.element_state === "liquid").length
  }

  onElementCountRadioButtonChange(selectedRadioButtonValue: string): void{
    this.selectedElementCountRadioButton = selectedRadioButtonValue;
  }
}
