import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementCardViewComponent } from './element-card-view.component';

describe('ElementCardViewComponent', () => {
  let component: ElementCardViewComponent;
  let fixture: ComponentFixture<ElementCardViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ ElementCardViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ElementCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
