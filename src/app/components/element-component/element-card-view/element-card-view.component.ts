import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IElement } from '../interfaces/element';
import { ShowElementGroupingComponent } from '../show-element-grouping/show-element-grouping.component';
import { ShowElementStateComponent } from '../show-element-state/show-element-state.component';

@Component({
  selector: 'app-element-card-view',
  standalone: true,
  imports: [CommonModule, RouterModule, ShowElementGroupingComponent, ShowElementStateComponent],
  templateUrl: './element-card-view.component.html',
  styleUrls: ['./element-card-view.component.css']
})
export class ElementCardViewComponent implements OnInit{

  @Input()
  element!: IElement;

  ngOnInit(): void {
    console.log("Here is the element data in the element-card-view.component: " + this.element);
  }

}
