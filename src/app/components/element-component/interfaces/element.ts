export interface IElement{
    atomic_number: number | null;
    atomic_weight: number | null;
    element_name: string | null;
    element_symbol: string | null;
    element_state: string | null;
    element_group: number | null;
    element_discovery: string | null;
    element_image: string |null;
}