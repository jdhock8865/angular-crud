import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowElementStateComponent } from './show-element-state.component';

describe('ShowElementStateComponent', () => {
  let component: ShowElementStateComponent;
  let fixture: ComponentFixture<ShowElementStateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowElementStateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowElementStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
