import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  standalone: true,
  selector: 'app-show-element-state',
  templateUrl: './show-element-state.component.html',
  styleUrls: ['./show-element-state.component.css'],
  imports: [CommonModule, FormsModule]
})
export class ShowElementStateComponent implements OnInit, OnChanges, OnDestroy {
  selectedRadioButtonValue: string = 'All';

  @Input()
  all!: number;

  @Input()
  solid!: number;

  @Input()
  liquid!: number;

  @Input()
  gas!: number;

  @Output()
  countRadioButtonSelectionChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor () {
    //console.log("The show-element-state component constructor has been called!")
  }

  ngOnInit(): void {
    //console.log("Count of all elements: " + this.all);
  }

  ngOnChanges(): void {}

  ngOnDestroy(): void {}

  onRadioButtonSelectionChange() {
    this.countRadioButtonSelectionChanged.emit(this.selectedRadioButtonValue);
    console.log(this.selectedRadioButtonValue);
  }
}
