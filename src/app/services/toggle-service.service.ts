import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToggleServiceService {

  tableView: boolean = true;
  
  constructor() { }

  toggleView(): void{
    this.tableView = !this.tableView;
  }
}
